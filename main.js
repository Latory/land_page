$(".arrow_top").click(function(){
    $("html,body").animate({
        scrollTop:$("header").offset().top
    },500);
});

$(".reasons").click(function(){
    $("ul").slideDown("slow");
})

var input = document.querySelector(".nav__search");
var button =document.querySelector(".nav__button");

button.onclick = () => {

    if(input.value =="Египет"){
        alert("Там верблюды плюются!");
    } else if(input.value =="Бали"){
        alert("У тебя там бумажник умыкнут");
    } else if(input.value =="Тайланд"){
        alert("Покусают огромные осы");
    }  else if(input.value ==""){
        alert("Введите название страны")
    }else {
        alert("Ничего не найдено");
    }
}

var quest = document.querySelectorAll(".faq-item-quest");

for(var i = 0; i < quest.length; i++){
quest[i].onclick = function () {
   if(this.parentNode.childNodes[3].style.display == "none"){
       this.parentNode.childNodes[3].style.display = "flex";
   } else{
       this.parentNode.childNodes[3].style.display = "none";
   }
}
}

let form_popup = document.getElementsByClassName("form-request-popup");
let form_request = document.querySelector(".form-request-button-box");
let content = document.querySelector(".form-request-popup-content")
let close_button = document.querySelector(".form-request-popup-close");
let submit_button = document.querySelector(".submit")
form_request.onclick = function () {


    if (form_popup[0].className == "form-request-popup hidden") {
        form_popup[0].className = "form-request-popup see bounceInDown animated 1s";
    } else{form_popup[0].className = "form-request-popup see bounceInDown animated 1s ";

    }
}

close_button.onclick = () =>{
    form_popup[0].className = "form-request-popup bounceOutDown animated  1s";
}
$("form").submit(function(e){
    e.preventDefault();

    let fioVal = $(this).find("[name='fio']").val();
    let emailVal = $(this).find("[name='email']").val();
    let phoneVal = $(this).find("[name='phone']").val();
    let errorElement = $(this).find(".error-message");

    if(fioVal == "" || emailVal == "" || phoneVal == ""){
        let errorMessage = "Необходимые поля не заполнены:";
    
    if(fioVal == ""){
        $(this).find("[name='fio']").css("border-color", "red");
        errorMessage = errorMessage + " фамилия; ";
    } else{
        $(this).find("[name='fio']").css("border-color", "green");
    }

    
    if(emailVal == ""){
        $(this).find("[name='email']").css("border-color", "red");
        errorMessage = errorMessage + " почта; ";
    } else{
        $(this).find("[name='email']").css("border-color", "green");
    }

    if(phoneVal == ""){
        $(this).find("[name='phone']").css("border-color", "red");
        errorMessage = errorMessage + " телефон. ";
    } else{
        $(this).find("[name='phone']").css("border-color", "green");
    }

    errorElement.html(errorMessage);
    errorElement.slideDown();
    }else{
        alert("Форма отправлена");
        errorElement.slideUp();
        $(this).find("[name='fio']").css("border-color", "green");
        $(this).find("[name='email']").css("border-color", "green");
        $(this).find("[name='phone']").css("border-color", "green");
    }
});

$("[name='fio'], [name='email'],[name='phone']").keyup(function(e){
    if(e.keyCode != 27 && e.keyCode != 9 && e.keyCode != 16 && e.keyCode != 17){
        if($(this).val().length >=2 && $(this).val().length <= 30){
            $(this).css("border-color", "green");
        }else{
            $(this).css("border-color", "red");
        }
    }
});

$(window).keydown(function(e){
    if(e.keyCode == 192){
        form_popup[0].className = "form-request-popup see bounceInDown animated 1s";
    }
});

$(window).keydown(function(e){
    if(e.keyCode == 27){
        form_popup[0].className = "form-request-popup bounceOutDown animated  1s";
    }
});

$(window).keydown(function(e){
    if(e.keyCode == 32 || e.keyCode == 190){
        $("[type='submit']").click();
    }
});

$(window).keydown(function(e){
    if(e.keyCode == 49){
        content.className = "alt_dark"; submit_button.className = "alt_dark_button"; close_button.className = "close_dark"
    };
    if(e.keyCode == 50){
        content.className = "alt_white"; submit_button.className = "alt_white_button"; close_button.className = "close_white"
    };
    if(e.keyCode == 51){
        content.className = "alt_flower"; submit_button.className = "alt_flower_button"; close_button.className = "close_flower"
    }
});





